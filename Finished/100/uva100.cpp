#include <iostream>

using namespace std;

#define SIZE 1000001

unsigned int *list;

long int solve(long int);

int main(void){
	long int i,max,low,up,tmp;
	list = new unsigned int[SIZE];
	list[1]=1;
	while(cin >> low >> up){
		max = 0;
		tmp = -1;
		if(low>up){
			tmp = low;
			low = up;
			up = tmp;
		}
		for(i=low;i<=up;i++){
			solve(i);
			if(list[i]>max) max = list[i];
		}
		if(tmp!=-1)
			cout << up << " " << low << " " << max << endl;
		else
			cout << low << " " << up << " " << max << endl;
	}
	delete []list;

	return 0;
}

long int solve(long int n){
	if(n>SIZE){
		if(n%2==0) return (solve(n>>1)+1);
		else return solve((3*n+1)>>1)+2;
	}
	if(list[n]!=0) return list[n];

	if(n%2==0) list[n]=solve(n>>1)+1;
	else list[n]=solve((3*n+1)>>1)+2;

	return list[n];
}

