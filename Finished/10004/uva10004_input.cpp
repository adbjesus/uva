#include <iostream>
#include <algorithm>
#include <stack>

using namespace std;

class node{
	public:
		int e;
		int edges[1000];
		int color;
		void addEdge(int);
};

void node::addEdge(int n){
	this->edges[this->e++]=n;
}

int failed,N;
node list[1000];

int main(void){
	int M,i,a,b;

	while(cin >> N && N){
		cin >> M;
		for(i=0;i<N;i++){
			list[i].color = 0;
			list[i].e=0;
		}
		failed = 0;
		for(i=0;i<M;i++){
			cin >> a >> b;
			if(list[a].color!=list[b].color){
				if(list[a].color==0)
					list[a].color = list[b].color*-1;
				else if(list[b].color==0)
					list[b].color = list[a].color*-1;
			}
			else if(list[a].color==list[b].color){
				if(list[a].color==0){
					list[a].color=1;
					list[b].color=-1;
				}
				else{
					failed=1;
					break;
				}
			}
		}
		
		for(i++;i<M;i++)
			cin >> a >> b;

		if(failed) cout << "NOT BICOLORABLE." << endl;
		else cout << "BICOLORABLE." << endl;
	}
	return 0;
}
