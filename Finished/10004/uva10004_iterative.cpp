#include <iostream>
#include <algorithm>
#include <stack>

using namespace std;

class node{
	public:
		int e;
		int edges[1000];
		int color;
		void addEdge(int);
};

void node::addEdge(int n){
	this->edges[this->e++]=n;
}

int failed,N;
node list[1000];
stack<int> check;

int main(void){
	int M,i,a,b;

	while(cin >> N && N){
		cin >> M;
		for(i=0;i<N;i++){
			list[i].color = 0;
			list[i].e=0;
		}
		failed = 0;
		for(i=0;i<M;i++){
			cin >> a >> b;
			list[a].addEdge(b);
			list[b].addEdge(a);
		}
		list[0].color=1;
		check.push(0);

		for(;!check.empty();){
            a = check.top();
            check.pop();
            for(i=0;i<list[a].e;i++){
                if(list[list[a].edges[i]].color==0){
                    if(list[a].color==1) list[list[a].edges[i]].color=-1;
                    else list[list[a].edges[i]].color=1;
                    check.push(list[a].edges[i]);
                }
                else if(list[a].color==list[list[a].edges[i]].color){
                    failed = 1;
                    while(!check.empty())
                        check.pop():
                    break;
                }
            }
		}
		if(failed) cout << "NOT BICOLORABLE." << endl;
		else cout << "BICOLORABLE." << endl;
	}
	return 0;
}
