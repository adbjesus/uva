#include <iostream>
#include <algorithm>

using namespace std;

class node{
	public:
		int e;
		int edges[1000];
		int color;
		void addEdge(int);
};

void node::addEdge(int n){
	this->edges[this->e++]=n;
}

void recursive(int,int);

int failed,N;
node list[1000];

int main(void){
	int M,i,a,b;

	while(cin >> N && N){
		cin >> M;
		for(i=0;i<N;i++){
			list[i].color = 0;
			list[i].e=0;
		}
		failed = 0;
		for(i=0;i<M;i++){
			cin >> a >> b;
			list[a].addEdge(b);
			list[b].addEdge(a);
		}
		recursive(0,0);
		if(failed) cout << "NOT BICOLORABLE." << endl;
		else cout << "BICOLORABLE." << endl;
	}
	return 0;
}

void recursive(int n,int last){
	int i;
	if(failed) return;
	if(list[n].color!=0){
		if(list[n].color==list[last].color)	failed=1;
		return;
	}
	if(list[last].color==1) list[n].color=-1;
	else list[n].color=1;
	for(i=0;i<list[n].e;i++)
		recursive(list[n].edges[i],n);
}
