#include <iostream>
#include <algorithm>
#include <cmath>
#include <iomanip>

using namespace std;

void closestpair(void);
double dist(int,int);

int T,N,C;
double list[100][3];
double res;

int main(void){
	int i,j;

	cin >> T;

	for(i=0;i<T;i++){
		cin >> N;
		C=0;
		res = 0;
		for(j=0;j<N;j++){
			cin >> list[j][0] >> list[j][1];
			list[j][2]=0;
		}
		for(j=0;j<N-1;j++)
			closestpair();
		cout << setprecision(2) << fixed << res << "\n";
		if(i!=T-1)
			cout << "\n";
	}


	return 0;
}

void closestpair(void){
	double min=-1;
	int i1,i2;
	int i,j;
	for(i=0;i<N;i++){
		if(C!=0){
			for(j=i+1;j<N;j++){
				if(list[i][2]==1 && list[j][2]==1)
					for(;j<N && list[j][2]==1;j++);
				else if(list[i][2]==0 && list[j][2]==0)
					for(;j<N && list[j][2]==0;j++);
				if(j==N) break;
				if(dist(i,j)<min || min==-1){
					i1 = i;
					i2 = j;
					min = dist(i,j);
				}
			}
		}
		else{
			for(j=i+1;j<N;j++){
				if(dist(i,j)<min || min==-1){
					i1 = i;
					i2 = j;
					min = dist(i,j);
				}
			}
		}
	}
	if(C==0) C=2;
	else C++;
	list[i1][2]=1;
	list[i2][2]=1;
	res+=min;
}

double dist(int ind1,int ind2){
	return ((sqrt(pow(list[ind1][0]-list[ind2][0],2)+pow(list[ind1][1]-list[ind2][1],2))));
}
	
