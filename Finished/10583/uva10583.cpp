#include <cstdio>
 
int group;
int set[50001];
int rank[50001];

void set_init(int);
int find(int);
void unite(int,int);

int main(){
    int n, m, i, j, count = 0;
    while (scanf("%d%d", &n, &m) != EOF && n && m) {
        group = n;
        set_init(n);
        while (m--) {
            scanf("%d%d", &i, &j);
            unite(i, j);
        }
        printf("Case %d: %d\n",++count,group);
    }
    return 0;
}

void set_init(int n){
    int i;
    for (i = 1; i <= n; i++) {
        set[i] = i;
        rank[i] = 1;
    }
}

int find(int x){
    if (set[x] == x)
        return x;
    else
        return set[x] = find(set[x]);
}

void unite(int a, int b){
    a = find(a);
    b = find(b);
    if (a == b) 
        return;
    group--;
   
	 	if (rank[a] < rank[b]) 
        set[a] = b;
	 	else {
        set[b] = a;
        if (rank[a] == rank[b])
            rank[a]++;
    }
}
