#include <iostream>
#include <queue>

using namespace std;

int t=0,v[50000];

void solve(queue<int> *q, int i){
	if (v[i]!=0) return;
	if (q[i].size()==0) return;
	++v[i];
	int x;
	while(q[i].size()){
		x=q[i].front();
		q[i].pop();
		solve(q,x);
	}
}

int main(){
	int N=1;
	int n=0,m=0,a=0,b=0;
	queue<int> q[50000];
	cin >> n >> m;
	while (n!=0){
		t=0;
		for (int i=0; i<m; ++i){
			cin >> a >> b;
			--a;
			--b;
			if (a>b) swap(a,b);
			if (a==b) continue;
			q[a].push(b);
			q[b].push(a);
		}
		for (int i=0; i<n; ++i){
			if (q[i].size()!=0) ++t;
			solve(q,i);
			while(q[i].size()){
				q[i].pop();
			}
		}
		for(int i=0; i<n; ++i)
			if(v[i]==0)
				++t;
		cout << "Case " << N++ << ": " << t << endl;
		cin >> n >> m;
		for (int i=0; i<n; ++i)
			v[i]=0;
	}
	return 0;
}
