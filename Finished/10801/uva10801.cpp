#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <queue>
#include <cmath>

#define SIZE 100

using namespace std;

int dijkstra(void);
int search(int,int);

int N,K;
int speed[5];
int floors[5][100];
int nfloors[5];
int dist[100];
int elev[100];

int main(void){
	int i,tmp,k,best;
	string input;

	while(cin >> N >> K){
		for(i=0;i<N;i++)
			cin >> speed[i];
		cin.ignore(1,'\n');
		for(i=0;i<N;i++){
			getline(cin,input);
			istringstream iss(input,istringstream::in);
			k=0;
			while(iss >> tmp)
				floors[i][k++] = tmp;
			nfloors[i]=k;
		}

		best = dijkstra();
		if(best==-1)
			cout << "IMPOSSIBLE" << endl;
		else
			cout << best << endl;
	}

	return 0;
}

int dijkstra(void){
	int i,node,j,d;
	queue<int> Q;
	int poss;
	int c;

	for(i=1;i<SIZE;i++)
		dist[i]=-1;
	dist[0]=0;
	elev[0]=-1;
	
	Q.push(0);

	while(!Q.empty()){
		node = Q.front();
		Q.pop();
		c = elev[node];
		for(i=0;i<N;i++){
			poss = search(i,node);
			if(poss!=-1)
				for(j=0;j<nfloors[i];j++){
					d = dist[node]+abs(floors[i][j]-node)*speed[i];
					if(c!=-1 && c!=i)
						d+=60;
					if(d < dist[floors[i][j]] || dist[floors[i][j]]==-1){
						dist[floors[i][j]]= d;
						elev[floors[i][j]]= i;
						Q.push(floors[i][j]);
					}
				}
		}
	}
	return dist[K];
}

int search(int ind,int node){
	int j;
	for(j=0;j<nfloors[ind];j++)
		if(floors[ind][j]==node) return j;
	return -1;
}
	
