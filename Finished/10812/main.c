#include <stdio.h>
#include <stdlib.h>

int main(void){
	int i,N,s,a;
	scanf("%d",&N);
	for(;N>0;N--){
		scanf("%d %d",&s,&a);
		i = s-a;
		if(i%2==0 && i>=0){
			i = i/2;
			printf("%d %d\n",s-i,i);
		}
		else printf("impossible\n");
	}
	return 0;
}
