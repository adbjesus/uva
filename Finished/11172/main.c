#include <stdio.h>
#include <stdlib.h>

int N;

void solve(){
	int n1,n2;

	scanf("%d %d",&n1,&n2);

	if(n1>n2)
		printf(">\n");
	else if(n1<n2)
		printf("<\n");
	else printf("=\n");

	return;

}

int main(void){
	scanf("%d",&N);

	for(;N>0;N--){
		solve();
	}	
	
	return 0;
}
