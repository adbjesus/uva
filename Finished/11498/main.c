#include <stdio.h>
#include <stdlib.h>

int main(void){
	int divx,divy,K,x,y;

	while(scanf("%d",&K)!=-1){
		scanf("%d %d",&divx,&divy);
		for(;K>0;K--){
			scanf("%d %d",&x,&y);
			if(x == divx || y == divy)
				printf("divisa\n");
			else if(x>divx && y>divy)
				printf("NE\n");
			else if(x>divx && y<divy)
				printf("SE\n");
			else if(x<divx && y<divy)
				printf("SO\n");
			else
				printf("NO\n");
		}

	}	
	
	return 0;
}
