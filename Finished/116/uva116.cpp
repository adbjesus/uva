#include <iostream>
#include <algorithm>

using namespace std;

long int min3(long int,long int,long int,long int);

long int lista[10001][10001];
long int best;

int main(void){
	long int i,k,N,M,m;
	while(cin >> N >> M){
		for(i=0;i<N;i++){
			for(k=0;k<M;k++){
				cin >> lista[i][k];
			}
		}
		if(N==1){
			i=0;
			for(k=0;k<M-1;k++){
				i+=lista[0][k];
				cout << 1 << " ";
			}
			i+=lista[0][M-1];
			cout << 1 << endl << i << endl;
			continue;
		}

		for(k=M-2;k>=0;k--){
			m = min3(0,1,N-1,k+1);
			lista[0][k]+=lista[m][k+1];
			for(i=1;i<N-1;i++){
				m = min3(i-1,i,i+1,k+1);
				lista[i][k]+=lista[m][k+1];
			}
			m=min3(0,N-2,N-1,k+1);
			lista[N-1][k]+=lista[m][k+1];
		}

		m = 0;
		for(i=1;i<N;i++)
			if(lista[i][0]<lista[m][0]) m=i;
		cout << m+1;
		best = lista[m][0];
		for(k=1;k<M;k++){
			cout << " ";
			if(m==0)
				m = min3(0,1,N-1,k);
			else if(m==N-1)
				m = min3(0,N-2,N-1,k);
			else
				m = min3(m-1,m,m+1,k);
			cout << m+1;
		}
		cout << endl << best << endl;
	}
	return 0;
}

long int min3(long int n1,long int n2,long int n3,long int j){
	if(lista[n1][j] <= lista[n2][j] && lista[n1][j] <= lista[n3][j]) return n1;
	else if(lista[n2][j] < lista[n1][j] && lista[n2][j] <= lista[n3][j]) return n2;
	return n3;
}
