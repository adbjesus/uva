#include <stdio.h>
#include <stdlib.h>

#define med(s1,s2,s3) ((((s1)>(s2) && (s1)<(s3)) || ((s1)<(s2) && (s1)>(s3)))?s1:((((s2)>(s1) && (s2)<(s3)) || ((s2)<(s1) && (s2)>(s3)))?s2:s3))

int main(void){
	int T,t,s1,s2,s3;
	scanf("%d",&T);
	for(t=1;t<=T;t++){
		scanf("%d %d %d",&s1,&s2,&s3);
		printf("Case %d: %d\n",t,med(s1,s2,s3));
	}
	return 0;
}
