#include <iostream>

using namespace std;

int main(void){
	long int NT[81];
	int N;
	int g;
	NT[1]=1;
	NT[2]=2;
	for(N=3;N<81;N++)
		NT[N]=NT[N-2]+NT[N-1];

	while(cin >> g && g)
		cout << NT[g] << endl;

	return 0;
}
