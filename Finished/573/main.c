#include <stdio.h>
#include <stdlib.h>

double H,U,D,F;

int main(void){
	double init,initU;
	int i,d;
	for(scanf("%lf %lf %lf %lf",&H,&U,&D,&F);H!=0;scanf("%lf %lf %lf %lf",&H,&U,&D,&F)){
		init = H;
		initU = U;
		for(i=0,d=1;;i++,d=!d){
			H -= U;
			U -= initU*(F/100);
			if(U<0) U=0;
			if(H<0){printf("success on day %d\n",i+1);break;}
			H += D;
			if(H>init){printf("failure on day %d\n",i+1);break;}
		}
	}
	
	return 0;
}

