#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>

using namespace std;

int solve(void);

int S,K;
string words_s[2000],words_k[2000];

int main(void){
	string tmp;
	stringstream tmp2;
	int i,size1,size2;

	while(getline(cin,tmp)){
		size1=tmp.length();
		for(i=0;i<(int)tmp.length();i++)
			if(tmp[i]<65 || (tmp[i]>90 && tmp[i]<97) || tmp[i]>122)
				tmp[i] = ' ';
		tmp2 << tmp;
		for(i=0;tmp2 >> tmp;i++)
			words_s[i] = tmp;
		S = i;
		tmp2.clear();
		getline(cin,tmp);
		size2=tmp.length();
		for(i=0;i<(int)tmp.length();i++)
			if(tmp[i]<65 || (tmp[i]>90 && tmp[i]<97) || tmp[i] > 122)
				tmp[i] = ' ';
		tmp2 << tmp;
		for(i=0;tmp2 >> tmp;i++)
			words_k[i] = tmp;
		K = i;
		tmp2.clear();
		if(size1==0 || size2==0)
			i=-1;
		else
			i = solve();

		if(i==-1) cout << "Blank!" << "\n";
		else cout << "Length of longest match: " << i << "\n";
	}
	
	return 0;
}

int solve(void){
	int max=0,i,j,k,l,o;

	for(i=0;i<S;i++){
		for(j=0;j<K;j++){
			if(words_s[i].compare(words_k[j])==0){
				for(l=1,k=j+1,o=i+1;k<K;k++,l++,o++);
				if(l > max) max=l;
			}
		}
	}

	return max;
}


